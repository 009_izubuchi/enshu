import socketserver
import re
import urllib.parse
import os
import fcntl
import glob
from datetime import datetime

code = 'iso-2022-JP'
CRLF = re.compile(r'\r\n|\r|\n')
res_anchor = re.compile(r'>>(\d+)')
url = re.compile(r'(https?://[\w/:%#\$&\?\(\)~\.=\+\-]+)')


class ServerHandler(socketserver.StreamRequestHandler):
    def handle(self):
        thread_list = sorted(glob.glob(os.path.join('log', '*.log')))
        thread_path = [
            '/' +
            os.path.splitext(
                os.path.basename(f))[0] for f in thread_list]
        header = ''
        body = ''
        req = self.request.recv(2048).decode(code).split()

        if req == []:
            return False
        ja_flag = True
        if 'Accept-Language:' in req:
            if req[req.index('Accept-Language:') + 1].split(',')[0] != 'ja':
                ja_flag = False
        uri = urllib.parse.urlparse(req[1])
        path = uri.path
        query = urllib.parse.parse_qs(uri.query)  # myname, valueがキーとして入っているはず
        params = {}
        if ja_flag is True:
            param_list = [('myname', '名無し'), ('value', ''),
                          ('color', 'black'), ('size', '3'), ('theread_name', '')]
        else:
            param_list = [('myname', 'nameless'), ('value', ''),
                          ('color', 'black'), ('size', '3'), ('theread_name', '')]
        for param_name, default_value in param_list:
            pvalue = query.get(param_name)
            params[param_name] = urllib.parse.unquote(
                pvalue[0], code) if pvalue is not None else default_value
        if path == '/':
            status = '200 OK'
            header = 'Content-Type: text/html; charset=' + code
            if params['theread_name'] != '':
                f = open(
                    os.path.join(
                        'log',
                        datetime.now().strftime('%Y%m%d%H%M%S') +
                        '.log'),
                    'x')
                f.write(params['theread_name'] + '\n')
                f.close()
                thread_list = sorted(glob.glob(os.path.join('log', '*.log')))
                thread_path = [
                    '/' +
                    os.path.splitext(
                        os.path.basename(f))[0] for f in thread_list]
                body = '<html>\n'
                body += '<head>\n'
                body += '<title></title>\n'
                body += '<meta http-equiv="Refresh" content="0.1;URL=./">\n'
                body += '</head>\n'
                body += '</html>'

            else:
                message = ''
                for i, (thread, path) in enumerate(
                        zip(thread_list, thread_path)):
                    with open(thread, 'r') as f:
                        fcntl.flock(f, fcntl.LOCK_EX)
                        readlines = f.readlines()
                        message += str(i + 1) + '：<a href=' + path + '>' + \
                            readlines[0] + '(' + str(len(readlines) - 1) + ')' + '</a><br>\n'
                if message == '':
                    message = 'まだスレッドはありません'if ja_flag else 'There are still no threads.'
                body = '<html>\n'
                body += '<head>\n'
                if ja_flag is True:
                    body += '<title>掲示板</title>\n'
                    body += '</head>\n'
                    body += '<body>' + '<h1 align="center">掲示板</h1>' + '<br>'
                    body += '<form method=get>'
                    body += '新規スレッド <input type=text name=theread_name value=こんにちは>'
                    body += '<input type=submit value=作成>'
                    body += '</form><hr>'
                    body += '<h2>スレッド一覧</h2>'
                else:
                    body += '<title>BBS</title>\n'
                    body += '</head>\n'
                    body += '<body>' + '<h1 align="center">BBS</h1>' + '<br>'
                    body += '<form method=get>'
                    body += 'Start a new thread <input type=text name=theread_name value=hello>'
                    body += '<input type=submit value=Post>'
                    body += '</form><hr>'
                    body += '<h2>Threads</h2>'
                body = body + message
                body = body + '</body></html>'

        elif path in thread_path:
            log_file_path = os.path.join('log', path[1::] + '.log')
            status = '200 OK'
            header = 'Content-Type: text/html; charset=' + code  # 文字エンコードを日本語に設定

            log = []
            with open(log_file_path, 'r') as f:
                fcntl.flock(f, fcntl.LOCK_EX)
                res_number = len(f.readlines())
            if params['value'] != '':
                params['value'] = re.sub(CRLF, '<br/>', params['value'])
                params['value'] = re.sub(
                    res_anchor, r'<a href="#\1">>>\1</a>', params['value'])
                params['value'] = re.sub(
                    url, r'<a href="\1">\1</a>', params['value'])
                log.append(
                    '<a id="' +
                    str(res_number) +
                    '">' +
                    str(res_number) +
                    ' </a>' +
                    '<b>' +
                    params['myname'] +
                    '</b>　(<I>' +
                    datetime.now().strftime('%Y-%m-%d %H:%M:%S') +
                    '</I>)<BR><font color=' +
                    params['color'] +
                    ' size=' +
                    params['size'] +
                    '>' +
                    params['value'] +
                    '</font></p><br>\n')

                with open(log_file_path, 'a') as f:
                    fcntl.flock(f, fcntl.LOCK_EX)
                    for l in log:
                        f.write(l)
                body = '<html>\n'
                body += '<head>\n'
                body += '<title></title>\n'
                body += '<meta http-equiv="Refresh" content="0.1;URL=.' + path + '">\n'
                body += '</head>\n'
                body += '</html>'

            else:
                message = ''
                thread_name = ''
                with open(log_file_path, 'r') as f:
                    fcntl.flock(f, fcntl.LOCK_EX)
                    readlines = f.readlines()
                    for l in readlines[-1:0:-1]:
                        message += l
                    thread_name = readlines[0]
                body = '<html>\n'
                body += '<head>\n'
                body += '<title>' + thread_name + '</title>\n'
                body += '</head>\n'
                body += '<body><h1>' + thread_name + '</h1><br>'
                body += '<form method=get>'
                if ja_flag is True:
                    body += '名前 <input type=text name=myname value=名無し>'
                    body += '<select name=color>\n'
                    body += '<option value=black'
                    if (params['color'] == 'black'):
                        body += ' selected'
                    body += '>黒</option>\n'
                    body += '<option value=red'
                    if (params['color'] == 'red'):
                        body += ' selected'
                    body += '>赤</option>\n'
                    body += '<option value=blue'
                    if (params['color'] == 'blue'):
                        body += ' selected'
                    body += '>青</option>\n'
                    body += '<option value=green'
                    if (params['color'] == 'green'):
                        body += ' selected'
                    body += '>緑</option>\n'
                else:
                    body += 'name <input type=text name=myname value=nameless>'
                    body += '<select name=color>\n'
                    body += '<option value=black'
                    if (params['color'] == 'black'):
                        body += ' selected'
                    body += '>black</option>\n'
                    body += '<option value=red'
                    if (params['color'] == 'red'):
                        body += ' selected'
                    body += '>red</option>\n'
                    body += '<option value=blue'
                    if (params['color'] == 'blue'):
                        body += ' selected'
                    body += '>blue</option>\n'
                    body += '<option value=green'
                    if (params['color'] == 'green'):
                        body += ' selected'
                    body += '>green</option>\n'
                body += '</select>\n'
                body += '<select name=size>\n'
                body += '<option value=1'
                if (params['size'] == '1'):
                    body += ' selected'
                body += '>1</option>\n'
                body += '<option value=2'
                if (params['size'] == '2'):
                    body += ' selected'
                body += '>2</option>\n'
                body += '<option value=3'
                if (params['size'] == '3'):
                    body += ' selected'
                body += '>3</option>\n'
                body += '<option value=4'
                if (params['size'] == '4'):
                    body += ' selected'
                body += '>4</option>\n'
                body += '<option value=5'
                if (params['size'] == '5'):
                    body += ' selected'
                body += '>5</option>\n'
                body += '<option value=6'
                if (params['size'] == '6'):
                    body += ' selected'
                body += '>6</option>\n'
                body += '<option value=7'
                if (params['size'] == '7'):
                    body += ' selected'
                body += '>7</option>\n'
                body += '</select><br>\n'
                if ja_flag is True:
                    body += 'コメント <textarea name=value cols=40 rows=3></textarea>'
                    body += '<input type=submit value=書き込む>'
                else:
                    body += 'comment <textarea name=value cols=40 rows=3></textarea>'
                    body += '<input type=submit value=Post>'
                body += '</form><hr>\n'
                body += message
                if ja_flag is True:
                    body += '<a href=/>戻る</a><br>\n'
                else:
                    body += '<a href=/>Back</a><br>\n'
                body = body + '</body></html>'
        else:
            status = '302 Moved'
            header = 'Location: /'
        self.wfile.write((
            'HTTP/1.0 ' +
            status +
            '\r\n' +
            header +
            '\r\n\r\n' +
            body).encode(code))


address = ('localhost', 19009)
Handler = ServerHandler
socketserver.ThreadingTCPServer.allow_reuse_address = True
with socketserver.ThreadingTCPServer(address, ServerHandler) as server:
    server.serve_forever()
